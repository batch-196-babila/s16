//alert("Hi!");

// Arithmetic Operators

let x = 1397;
let y = 7831;
let sum = x + y;

console.log("The sum is " + sum);

let difference = y - x;
console.log("Subtraction operator " + difference);

let product = x * y;
console.log("Multiplication operator " + product);

let quotient = x / y;
console.log("Division operator " + quotient);

let reminder = y % x;
console.log("Modulo operator " + reminder); // 846

// Assignment Operator ( = )

let assingmentNumber = 8;
assingmentNumber += 2;
console.log("Addition assignment " + assingmentNumber);

assingmentNumber += 2;
console.log("Addition assignment " + assingmentNumber);

let string1 = "Boston";
let string2 = "Celtics";
string1 += string2;
console.log(string1);

assingmentNumber -= 2;
console.log("Addition assignment " + assingmentNumber);

assingmentNumber *= 2;
console.log("Addition assignment " + assingmentNumber);

assingmentNumber /= 2;
console.log("Addition assignment " + assingmentNumber);

// Multiple operators and parenthesis

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas); // 0.6


let pemdas = 1 + (2 - 3) * (4 / 5);
console.log(pemdas); // 0.199

// Increment and Decrement

let z = 1;
let increment = ++z;
console.log("Pre-increment: " + increment);
console.log("Value of z: " + z);

increment = z++;
console.log("Post-increment: " + increment);
console.log("Value of z: " + z); // return the value first before adding


let decrement = --z;
console.log("Pre-decrement: " + decrement);
console.log("Value of z: " + z);

increment = z--;
console.log("Post-increment: " + decrement);
console.log("Value of z: " + z);

// Type Coercion
// implicity = we can't see it. JS will do it behind

let numA = "10";
let numB = 12;
let coercion = numA + numB;
console.log(coercion); // concatenate 1012
console.log(typeof coercion); // result string

let numC = 16;
let numD = 14;
let numCoercion = numC + numD;
console.log(numCoercion);
console.log(typeof numCoercion);

let numE = true + 1;
console.log(numE); // result 2 = true is equivalent to 1

let numF = false + 1;
console.log(numF); 

// Equality Operator ( == ) - not strick
// check value to return true regardless of data type
console.log("Equality");
console.log(1 == 1); // true
console.log(1 == 2); // false
console.log(1 == "1"); // true
console.log(false == 0); // true
console.log("johnny" == "johnny"); // true
console.log("Johnny" == "johnny"); // false - case sensitive

// Inequality Operator ( != ) not equal
console.log("Inequality");
console.log(1 != 1); // false
console.log(1 != 2); // true
console.log(1 != "1"); // false
console.log(0 != false); // false
console.log("johnny" != "johnny"); // false
console.log("Johnny" != "johnny"); // true

// Strick Equality Operator ( === )
// check both value and data type to return True
console.log("Strict Equality");
console.log(1 === 1); // true
console.log(1 === "1"); //false
console.log(false === 0); // true

let johnny = "Johnny";
console.log("Johnny" === johnny);

console.log("Strict Inequality"); // ( !== )
console.log(1 !== 1); // false
console.log(1 !== 12);  // true
console.log(1 !== "1"); // true
console.log("Johnny" !== johnny); // false


// Relational Operator < > <= >=
console.log("Relational Operator");
let a = 50;
let b = 65;

// Greater Than
console.log(a > b); // false
console.log(a < b); // true
console.log(a >= b); // false
console.log(a <= b); // true

// forced coercion

let numStr = "30";
console.log(a > numStr); // true

let str = 'twenty';
console.log(b >= str);

// logical operator
// AND &&
// OR ||


let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

console.log("AND Operator");
let authorization = isAdmin && isRegistered;
console.log(authorization);

let authorization2 = isLegalAge && isRegistered
console.log(authorization2); // true

let authorization3 = isAdmin && isLegalAge;
console.log(authorization3); // false

let random = isAdmin && false; // false
console.log(random);

let requiredLevel = 95;
let requiredAge = 18;
let authorization4 = isRegistered && requiredLevel === 25; // false
console.log(authorization4);

let authorization5 = isRegistered && isLegalAge && requiredLevel === 95;
console.log(authorization5);

let userName = "gamer2022";
let userName2 = "theTinker";
let userAge = 15;
let userAge2 = 26;

let registration1 = userName.length > 8 && userAge >= requiredAge;
console.log(registration1);

let registration2 = userName2.length > 8 && userAge2 >= requiredAge;
console.log(registration2);

console.log("OR Operator || "); 

let userLevel = 100;
let userLevel2 = 65;

let guildRequirment = isRegistered && userLevel >= requiredLevel && userAge >= requiredAge;
console.log(guildRequirment); // false

let guildRequirment2 = isRegistered || userLevel2 >= requiredLevel || userAge2 >= requiredAge;
console.log(guildRequirment2); // true

let guildRequirment3 = userLevel >= requiredLevel || userAge >= requiredAge;
console.log(guildRequirment3); // true

let guildAdmin = isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin); // false

// Not Operator (!)
console.log("Not Operator");
console.log(!isRegistered); // false

let opposite1 = !isAdmin;
console.log(opposite1);